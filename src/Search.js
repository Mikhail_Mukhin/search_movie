import { Button, InputGroup, Form } from "react-bootstrap";

import React from "react";

const Search = (props) => {
  return (
    <div className="search">
      <InputGroup className="search-group">
        <InputGroup.Prepend>
          <InputGroup.Text id="search-group__Text">
            Search by (enter title, year or genre)
          </InputGroup.Text>
        </InputGroup.Prepend>
        <Form.Control
          className="search-group__input"
          aria-label="Default"
          aria-describedby="inputGroup-sizing-default"
          onChange={props.handleInputSearch}
          type="text"
        />
      </InputGroup>

      <Button className="search__button" variant="info">
        Search
      </Button>
    </div>
  );
};

export default Search;
