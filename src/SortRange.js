import React from "react";
import { useState } from "react";

const SortRange = (props) => {
  const [state, setState] = useState({
    yearStart: "1900",
    yearEnd: "2050",
  });

  function handleChange({ target: { name, value } }) {
    setState((prev) => ({ ...prev, [name]: value }));
  }

  return (
    <div>
      <div className="year-filter">
        <div className="sort-range">
          <h2 className="sort-range__title">range of years</h2>
          <input
            className="sort-range__input"
            type="number"
            placeholder="from"
            value={state.yearStart}
            name="yearStart"
            onChange={handleChange}
          />
          <input
            className="sort-range__input"
            type="number"
            placeholder="to"
            value={state.yearEnd}
            name="yearEnd"
            onChange={handleChange}
          />
          <div className="sort-range__append">
            <button
              className="btn sort-range__button"
              type="button"
              onClick={props.passYear(state)}
            >
              Button
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SortRange;
