import React from "react";
import ModalWindow from "./ModalWindow";

const Catalog = (props) => {
  return (
    <div className="movie-block">
      {props.movies.map((movie, idx) => {
        return (
          <div
            key={idx}
            className="movie-content"
            data-toggle="modal"
            data-target="#modalWindow"
            onClick={() => props.openPopup(movie.id)}
          >
            {movie.modal ? (
              <ModalWindow
                closeModal={(e) => props.closePopup(e, movie.id)}
                addToWatch={(id) => props.addToWatch(id)}
                movie={movie}
              />
            ) : (
              ""
            )}
            <img
              className="movie-content__image"
              src={movie.image}
              alt="#"
            ></img>
            <div className="movie-content__additional-information">
              <h1 className="movie-content__title">{movie.title}</h1>
              <p className="movie-content__year">year: {movie.year}</p>
              <p className="movie-content__rating">rating: {movie.rating}</p>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Catalog;
