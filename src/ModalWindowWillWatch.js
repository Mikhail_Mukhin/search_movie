import React from "react";

const ModalWindowWillWatch = (props) => {
  return (
    <div
      className="modalWindow"
      class="modal fade"
      id="modalWillwatch"
      tabindex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div className="movie-content-modal">
              <img
                className="movie-content__image-modal"
                src={props.movie.image}
                alt="#"
              ></img>
              <div className="movie-content__additional-information-modal">
                <h1 className="movie-content__title-modal">
                  {props.movie.title}
                </h1>
                <h2 className="movie-content__about-film-modal">about film:</h2>
                <p className="movie-content__country-modal">
                  country: {props.movie.country}
                </p>
                <p className="movie-content__producer-modal">
                  producer: {props.movie.producer}
                </p>
                <p className="movie-content__rating-modal">
                  rating: {props.movie.rating}
                </p>
                <p className="movie-content__year-modal">
                  year: {props.movie.year}
                </p>
                <p className="movie-content__gener-modal">
                  gener: {props.movie.gener}
                </p>
                <p className="movie-content__time-modal">
                  time: {props.movie.time}
                </p>
              </div>
              <div className="movie-content__actors-block-modal">
                <h3 className="movie-content__actors-header-modal">actors:</h3>
                {props.movie.actors.map((actor) => (
                  <ul>
                    <li className="movie-content__actors-elem">{actor}</li>
                  </ul>
                ))}
              </div>
              <button
                className="modalWindow__button btn btn-danger"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={props.closeModal}
              >
                close
              </button>
              <div className="movie-content__description-modal">
                <h3 className="movie-content__description-header-modal">
                  description:
                </h3>
                <p className="movie-content__description-text-modal">
                  {" "}
                  {props.movie.desctiption}{" "}
                </p>
              </div>
              <button
                className="movie-content__delete-button btn btn-danger"
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => props.deleteMovie(props.movie.id)}
              >
                delete
              </button>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>
      </div>
    </div>
  );
};

export default ModalWindowWillWatch;
