import React from "react";
import ReactDOM from "react-dom";
import "./styles/main.sass";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "bootstrap";

import "bootstrap/dist/js/bootstrap.js";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
