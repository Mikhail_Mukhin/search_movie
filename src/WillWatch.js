import React from "react";
import ModalWindowWillWatch from "./ModalWindowWillWatch";
import Search from "./Search";
import { useState } from "react";
import { useEffect } from "react";
import willWatchIcon from "../../search_movie/src/images/willWatch.jpg";

const WillWatch = (props) => {
  useEffect(() => {
    setLocalModal(
      props.willWatch.map((item) => {
        item = { ...item, modal: false };
        return item;
      })
    );
  }, [props.willWatch]);

  const [localModal, setLocalModal] = useState([]);

  const [searchMovie, setSearchMovie] = useState("");

  const [showWillWatchList, SetShowWillWatchList] = useState(false);

  const deleteMovie = (id) => {
    setLocalModal(
      localModal.map((movie, idx) =>
        id === movie.id ? localModal.splice(idx, 1) : movie
      )
    );
    props.changeWillWatchLength(localModal);
  };

  const willWatchShow = () => {
    SetShowWillWatchList(!showWillWatchList);
  };

  const openPopWillWatch = (id) => {
    setLocalModal(
      localModal.map((item) =>
        item.id === id
          ? (item = { ...item, modal: true })
          : (item = { ...item, modal: false })
      )
    );
  };

  const closePopWillWatch = (e) => {
    e.stopPropagation();
    setLocalModal(localModal.map((item) => (item = { ...item, modal: false })));
  };

  const handleInputSearch = (e) => {
    setSearchMovie(e.target.value);
  };

  const filteredMovieList = localModal.filter((item) => {
    return (
      item.title.toLowerCase().includes(searchMovie.toLowerCase()) ||
      item.year.toLowerCase().includes(searchMovie.toLowerCase()) ||
      item.rating.toLowerCase().includes(searchMovie.toLowerCase()) ||
      item.country.toLowerCase().includes(searchMovie.toLowerCase()) ||
      item.producer.toLowerCase().includes(searchMovie.toLowerCase()) ||
      item.actors.some((actor) =>
        actor.toLowerCase().includes(searchMovie.toLowerCase())
      )
    );
  });

  return (
    <div className="will-watch-block">
      <div className="will-watch__icon" onClick={willWatchShow}>
        <a className="will-watch__link" href="#">
          <img src={willWatchIcon} alt="will-watch" />
          <div className="will-watch__circle">
            <p className="will-watch__circle-text">
              {props.willWatchQuantity}{" "}
            </p>
          </div>
        </a>
      </div>

      {props.willWatch.length === 0 ? (
        <p className="will-watch__no-films">"you don't have movies to watch"</p>
      ) : showWillWatchList ? (
        <Search handleInputSearch={handleInputSearch} />
      ) : (
        ""
      )}

      <div className="movie-content-block">
        {showWillWatchList
          ? filteredMovieList.map((movie) => {
              return (
                <div
                  className="movie-content movie-content--will-watch"
                  data-toggle="modal"
                  data-target="#modalWillwatch"
                  onClick={() => openPopWillWatch(movie.id)}
                >
                  {movie.modal ? (
                    <ModalWindowWillWatch
                      closeModal={(e) => closePopWillWatch(e, movie.id)}
                      movie={movie}
                      deleteMovie={(id) => deleteMovie(id)}
                    />
                  ) : (
                    ""
                  )}
                  <img
                    className="movie-content__image"
                    src={movie.image}
                    alt="#"
                  ></img>

                  <div className="movie-content__additional-information">
                    <h1 className="movie-content__title">{movie.title}</h1>
                    <p className="movie-content__year">year: {movie.year}</p>
                    {/* <p className="movie-content__genre">gener: {movie.gener}</p> */}
                    <p className="movie-content__rating">
                      rating: {movie.rating}
                    </p>
                  </div>
                </div>
              );
            })
          : ""}
      </div>
    </div>
  );
};

export default WillWatch;
