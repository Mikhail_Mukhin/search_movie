import React from "react";
import { useState } from "react";

const Sort = (props) => {
  const [sortOpton, setSortOption] = useState("");

  const handleChange = (e) => {
    setSortOption(e.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    props.passSortValue(sortOpton);
  };
  return (
    <div className="sort-descending__block">
      <form className="sort-descending__form" onSubmit={handleSubmit}>
        <label className="sort-descending__lable">
          sort by rating or year (descending)
          <select
            className="sort-descending__select"
            value={sortOpton}
            onChange={(e) => handleChange(e)}
          >
            <option value="not sorted">not sorted</option>
            <option value="year">year</option>
            <option value="rating">rating</option>
          </select>
        </label>
        <button
          className="sort-descending__button btn btn-info"
          type="submit"
          value="sort"
        >
          Sort
        </button>
      </form>
    </div>
  );
};

export default Sort;
