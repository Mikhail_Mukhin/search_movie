import { useState } from "react";
import Search from "./Search";
import Catalog from "./Catalog";
import SortDescending from "./SortDescending";
import SortRange from "./SortRange";
import WillWatch from "./WillWatch";
import image1 from "../../search_movie/src/images/1.jpg";
import image2 from "../../search_movie/src/images/2.jpg";
import image3 from "../../search_movie/src/images/3.jpg";
import image4 from "../../search_movie/src/images/4.jpg";
import image5 from "../../search_movie/src/images/5.jpg";
import image6 from "../../search_movie/src/images/6.jpg";
import image7 from "../../search_movie/src/images/7.jpg";
import image8 from "../../search_movie/src/images/8.jpg";
import image9 from "../../search_movie/src/images/9.jpg";
import image10 from "../../search_movie/src/images/10.jpg";
import image11 from "../../search_movie/src/images/11.jpg";
import image12 from "../../search_movie/src/images/12.jpg";
import image13 from "../../search_movie/src/images/13.jpg";
import image14 from "../../search_movie/src/images/14.jpg";
import image15 from "../../search_movie/src/images/15.jpg";
import image16 from "../../search_movie/src/images/16.jpg";
import image17 from "../../search_movie/src/images/17.jpg";
import image18 from "../../search_movie/src/images/18.jpg";
import image19 from "../../search_movie/src/images/19.jpg";
import image20 from "../../search_movie/src/images/20.jpg";

function App() {
  const [movies, setMovies] = useState([
    {
      key: new Date(),
      id: new Date(),
      image: image1,
      title: "The shawshank redemption",
      country: "USA",
      producer: "Frank Darabont",
      time: 142,
      actors: ["Tim Robbins", "Morgan Freeman", "Bob Gunton"],
      gener: "drama",
      year: "1994",
      rating: "9.1",
      desctiption: `Accountant Andy Dufrein is charged with the murder of his 
      own wife and her lover. Once in a prison called Shawshank, he is faced 
      with the brutality and lawlessness that reigns on both sides of the bars. 
      Everyone who falls into these walls becomes their slave for the rest of 
      their lives. But Andy, with a lively mind and a kind soul, finds an 
      approach to both prisoners and guards, seeking their special favor.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image2,
      title: "Green mile",
      gener: "fantasy, drama",
      country: "USA",
      producer: "Frank Darabont",
      time: 189,
      actors: ["Tom Hanks", "David Morse", "Bonnie Hunt"],
      year: "1999",
      rating: "9.0",
      desctiption: `Paul Edgecombe is the death squad leader at Cold Mountain 
      Prison, each of whose prisoners once walks the Green Mile on their way 
      to the execution site. Paul saw many inmates and guards during his work. 
      However, the giant John Coffey, accused of a terrible crime, became one 
      of the most unusual inhabitants of the block.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image3,
      title: "Lord of the ring",
      gener: "fantasy, adventure",
      country: "New Zealand",
      producer: "Peter Jackson",
      time: 201,
      actors: ["Elijah Wood", "Viggo Mortensen", "Sean Astin"],
      year: "2003",
      rating: "8.6",
      desctiption: `The last part of the trilogy about the Ring of Omnipotence 
      and about the heroes who took on the burden of saving Middle-earth. The 
      lord of the forces of Darkness Sauron directs his countless armies under 
      the walls of Minas Tirith, the fortress of Light's Hope. He looks forward 
      to a close victory, but this is what prevents him from noticing two tiny 
      figures - hobbits approaching Mount Doom, where they have to destroy the 
      Ring of Omnipotence. Will happiness smile at them?`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image4,
      title: "Interstellar",
      gener: "science fiction, drama",
      country: "USA",
      producer: "Christopher Nolan",
      time: 169,
      actors: ["Matthew McConaughey", "Ann Hataway", "Jessica Chastain"],
      year: "2014",
      rating: "8.6",
      desctiption: `When droughts, dust storms, and plant extinctions lead 
      humanity to a food crisis, a team of researchers and scientists is sent 
      through a wormhole (which supposedly connects regions of space-time 
        across a great distance) on a journey to surpass the previous 
        restrictions on human space travel and find a planet with suitable 
        humanity conditions.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image5,
      title: "Shindler's list",
      gener: "drama, biography",
      country: "USA",
      producer: "Steven Spielberg",
      time: 195,
      actors: ["Liam Neeson", "Ben Kingsley", "Rafe fiennes"],
      year: "1993",
      rating: "8.8",
      desctiption: `The film tells the real story of the enigmatic Oskar 
      Schindler, a member of the Nazi party, a successful manufacturer who 
      saved nearly 1,200 Jews during World War II.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image6,
      title: "Intocable",
      gener: "drama, comedy",
      country: "France",
      producer: "Olivier Nakache",
      time: 112,
      actors: ["Francois Cluse", "Omar Sy", "Anne Le Ny"],
      year: "2011",
      rating: "8.8",
      desctiption: `Suffering from an accident, the wealthy aristocrat Philip 
      hires as his assistant the man who is least suited for the job - a young 
      resident of the Drissa suburb who has just been released from prison. 
      Despite the fact that Philip is confined to a wheelchair, Driss manages 
      to bring the spirit of adventure into the measured life of an 
      aristocrat.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image7,
      title: "Back to the future",
      gener: "science fiction, comedy",
      country: "USA",
      producer: "Robert Zemeckis",
      time: 116,
      actors: ["Michael J. Fox", "Christopher Lloyd", "Leah Thompson"],
      year: "1985",
      rating: "8.6",
      desctiption: `The teenager Marty, with the help of a time machine built 
      by his friend professor Doc Brown, gets from the 80s to the distant 50s. 
      There he meets his future parents, still teenagers, and a professor 
      friend, very young.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image8,
      title: "Klaus",
      gener: "cartoon",
      country: "Spain",
      producer: "Sergio Pablos",
      time: 96,
      actors: ["Jason Schwartzman", "J.K. Simmons", "Joan Cusack"],
      year: "2019",
      rating: "8.7",
      desctiption: `The owner of the postal empire, in order to teach a lazy 
      offspring named Jesper wisdom, sends him to the far north to the city 
      of Smirensburg. He must organize a post office there and process at 
      least 6,000 letters in a year. Arriving at the place, the guy finds 
      himself in a war zone: two family clans, as it happened historically, 
      harbor mutual hatred and sacredly honor the centuries-old tradition 
      at every opportunity to mutuz each other and do dirty tricks.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image9,
      title: "coco",
      gener: "cartoon, fantasy",
      country: "USA",
      producer: "Lee Ankrich",
      time: 105,
      actors: ["Anthony Gonzalez", "Gael Garcia Bernal", "Benjamin Bratt"],
      year: "2017",
      rating: "8.6",
      desctiption: `12-year-old Miguel lives in a Mexican village in a family 
      of shoemakers and secretly dreams of becoming a musician. Secretly - 
      because in his family clan, music is considered a curse. Once, his 
      great-great-grandfather left his wife, Miguel's great-great-grandmother, 
      for the sake of a dream, which now does not allow Miguel to live in 
      peace. Since then, the theme of music in the family has become a taboo. 
      Miguel discovers that there is some - as yet unnamed - connection between 
      him and his beloved singer Ernesto de la Cruz, now deceased.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image10,
      title: "snatch",
      gener: "crime, comedy",
      country: "Great Britain",
      producer: "Guy Ritchie",
      time: 104,
      actors: ["Jason Statham", "Stephen Graham", "Brad Pitt"],
      year: "2000",
      rating: "8.5",
      desctiption: `
      Four-fingered Frankie was supposed to ship a stolen diamond from 
      England to the United States to his boss Evie, but, having made a 
      bet on an underground boxing match, he gets into a whirlpool of very 
      undesirable events.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image11,
      title: "pulp fiction",
      gener: "thriller, comedy",
      country: "USA",
      producer: "Quentin Tarantino",
      time: 154,
      actors: ["John Travolta", "Samuel L. Jackson", "Bruce willis"],
      year: "1994",
      rating: "8.6",
      desctiption: `Two bandits Vincent Vega and Jules Winfield have 
      philosophical conversations in between showdowns and solving problems 
      with debtors of crime boss Marcellus Wallace.

      In the first story, Vincent spends an unforgettable evening with 
      Marcellas' wife Mia. The second tells about the boxer Butch Coolidge, 
      bought by Wallace to surrender the fight. In the third story, Vincent 
      and Jules accidentally get into trouble.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image12,
      title: "Shutter Island",
      gener: "detective, thriller",
      country: "USA",
      producer: "Martin Scorsese",
      time: 138,
      actors: ["Leonardo DiCaprio", "Mark Ruffalo", "Ben Kingsley"],
      year: "2009",
      rating: "8.5",
      desctiption: `Two American bailiffs are sent to one of the islands 
      in Massachusetts to investigate the disappearance of a patient of 
      a mental hospital. In their investigation, they will have to face a 
      web of lies, a hurricane that struck and a deadly riot of the clinic's 
      inhabitants.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image13,
      title: "inception",
      gener: "science fiction, action movie",
      country: "USA",
      producer: "Christopher Nolan",
      time: 148,
      actors: ["Leonardo DiCaprio", "Joseph Gordon-Levitt", "Elliot Page"],
      year: "2010",
      rating: "8.7",
      desctiption: `Cobb is a talented thief, the best of the best in the 
      dangerous art of extraction: he steals valuable secrets from the depths 
      of the subconscious during sleep, when the human mind is most vulnerable. 
      Cobb's rare abilities made him a valuable player in the betrayal world of 
      industrial espionage, but they also turned him into an eternal fugitive 
      and robbed him of everything he had ever loved.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image14,
      title: "the gentelemen",
      gener: "crime, comedy",
      country: "Great Britain",
      producer: "Guy Ritchie",
      time: 113,
      actors: ["Matthew McConaughey", "Charlie Hunnam", "Henry Golding"],
      year: "2019",
      rating: "8.5",
      desctiption: `One cunning American had been selling drugs since his 
      student days, and now he came up with a scheme of illegal enrichment 
      using the estates of the impoverished English aristocracy and became 
      very rich on this. Another nosy journalist comes to Ray, the American's 
      right hand, and offers him to buy a script that details the crimes of 
      his boss with the participation of other representatives of the London 
      criminal world - a Jewish partner, the Chinese diaspora, black athletes 
      and even a Russian oligarch.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image15,
      title: "the matrix",
      gener: "science fiction, action movie",
      country: "USA",
      producer: "Lana Wachowski",
      time: 136,
      actors: ["Keanu Reeves", "Laurence Fishburne", "Carrie-Anne Moss"],
      year: "1999",
      rating: "8.5",
      desctiption: `Thomas Anderson's life is divided into two parts: 
      during the day he is an ordinary office worker who receives scolding 
      from his superiors, and at night he turns into a hacker named Neo, 
      and there is no place on the network where he could not reach.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image16,
      title: "green book",
      gener: "drama, comedy",
      country: "USA",
      producer: "Peter Farrelly",
      time: 130,
      actors: ["Viggo Mortensen", "Mahershala Ali", "Linda Cardellini"],
      year: "2018",
      rating: "8.3",
      desctiption: `1960s. After the closure of a New York nightclub for 
      renovations, bouncer Tony, nicknamed Chatterbox, is looking for a 
      part-time job for a couple of months. Just at this time, Don 
      Shirley, a sophisticated socialite, a wealthy and talented black 
      musician who plays classical music, is going on a tour of the 
      southern states, where racist beliefs and segregation reigns. He 
      hires Tony as a driver, bodyguard, and problem-solving man. The 
      two have so little in common and this trip will change the lives 
      of both forever.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image17,
      title: "your name",
      gener: "anime, cartoon",
      country: "Japan",
      producer: "Makoto Shinkai",
      time: 110,
      actors: ["Ryunosuke Kamiki", "Mone Kamishiraishi", "Ryo Narita"],
      year: "2016",
      rating: "8.3",
      desctiption: `A Tokyo boy Taki and a girl from Mitsuha province 
      discover that there is a strange connection between them. In a 
      dream, they change bodies and live each other's lives. But one 
      day this ability disappears as suddenly as it appeared. Taki 
      decides to find Mitsuha by all means.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image18,
      title: "the Truman show",
      gener: "science fiction, drama",
      country: "USA",
      producer: "Peter Weir",
      time: 103,
      actors: ["Jim carrey", "Laura Linney", "Noah Emmerich"],
      year: "1998",
      rating: "8.3",
      desctiption: `Imagine that you suddenly begin to realize 
      that everything around you is scenery, and people are actors, 
      pretending to be who they seem to you. Your whole world turns 
      out to be a big television series where you play the main role 
      without even knowing it. Your entire life is the result of the 
      work of the author of a TV show that has been watched by the 
      entire planet for thirty years, starting from the moment you 
      were born.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image19,
      title: "the wolf of wall street",
      country: "USA",
      producer: "Martin Scorsese",
      time: 180,
      actors: ["Leonardo DiCaprio", "Jonah Hill", "Margot Robbie"],
      gener: "crime, drama",
      year: "2013",
      rating: "7.9",
      desctiption: `1987 year. Jordan Belfort becomes a broker for 
      a successful investment bank. The bank will close shortly after 
      the sudden collapse of the Dow Jones. On the advice of his wife 
      Teresa, Jordan gets a job in a small institution that deals with 
      small shares. His assertive customer communication style and 
      innate charisma quickly pays off. He meets Donnie's housemate, 
      a merchant, who immediately finds a common language with Jordan 
      and decides to open his own company with him.`,
      modal: false,
    },
    {
      key: new Date(),
      id: new Date(),
      image: image20,
      title: "the game",
      gener: "triller, drama",
      country: "USA",
      producer: "David Fincher",
      time: 129,
      actors: ["Michael Douglas", "Sean Penn", "Deborah Kara Unger"],
      year: "1997",
      rating: "8.3",
      desctiption: `Nicholas Van Orton is the epitome of success. 
      He succeeds, he is calm and calm, used to keeping any situation 
      under control. On his birthday, Nicholas receives an unusual 
      gift - a ticket to participate in the 'Game'. He is promised 
      that the game will return vivid feelings, allow him to feel 
      the taste and acuteness of life. Having entered the game, 
      Nicholas begins to realize that this is a game seriously, 
      a game not for life, but for death.`,
      modal: false,
    },
  ]);

  const [willWatch, setWillWatch] = useState([]);

  const [option, setOption] = useState("");

  const [year, setYear] = useState({
    yearStart: "1900",
    yearEnd: "2050",
  });

  const [searchMovie, setSearchMovie] = useState("");

  const [showMoreSearchOption, setShowMoreSearchOption] = useState(false);

  const geners = [
    "all",
    "drama",
    "fantasy",
    "adventure",
    "science fiction",
    "biography",
    "comedy",
    "detective",
    "thriller",
    "cartoon",
    "action",
    "anime",
  ];

  const [selectedGeners, setSelectedGeners] = useState("");

  const addToWatch = (id) => {
    willWatch.every((movie) => movie.id !== id)
      ? setWillWatch([
          ...willWatch,
          ...movies.filter((movie) => movie.id === id),
        ])
      : setWillWatch([...willWatch]);
  };

  const changeWillWatchLength = (newWillWatch) => {
    setWillWatch(newWillWatch);
  };

  const openPopup = (id) => {
    setMovies(
      movies.map((item) =>
        item.id === id
          ? (item = { ...item, modal: true })
          : (item = { ...item, modal: false })
      )
    );
  };

  const closePopup = (e) => {
    e.stopPropagation();
    setMovies(movies.map((item) => (item = { ...item, modal: false })));
  };

  const getSortValue = (sortOption) => {
    setOption(sortOption);
  };

  const getYear = (year) => {
    setYear(year);
  };

  const handleInputSearch = (e) => {
    setSearchMovie(e.target.value);
  };

  const handleChange = ({ target: { name, value } }) => {
    setSelectedGeners((prev) => ({ ...prev, [name]: value }));
  };

  const showMore = () => {
    setShowMoreSearchOption(!showMoreSearchOption);
  };

  const filteredMovieList = movies.filter((item) => {
    return (
      item.title.toLowerCase().includes(searchMovie.toLowerCase()) ||
      item.year.toLowerCase().includes(searchMovie.toLowerCase()) ||
      item.rating.toLowerCase().includes(searchMovie.toLowerCase()) ||
      item.country.toLowerCase().includes(searchMovie.toLowerCase()) ||
      item.producer.toLowerCase().includes(searchMovie.toLowerCase()) ||
      item.actors.some((actor) =>
        actor.toLowerCase().includes(searchMovie.toLowerCase())
      )
    );
  });

  if (option === "year") {
    movies.sort((a, b) => {
      return b.year - a.year;
    });
  } else if (option === "rating") {
    movies.sort((a, b) => {
      return b.rating - a.rating;
    });
  }

  const inputSearch = filteredMovieList.filter((item) => {
    return selectedGeners.gener && selectedGeners.gener !== "all"
      ? item.year >= year.yearStart &&
          item.year <= year.yearEnd &&
          item.gener.includes(selectedGeners.gener)
      : item.year >= year.yearStart && item.year <= year.yearEnd;
  });

  return (
    <div className="App">
      <h1 className="search__header">Search movie app</h1>
      <WillWatch
        willWatch={willWatch}
        movies={movies}
        willWatchQuantity={willWatch.length}
        changeWillWatchLength={changeWillWatchLength}
      />
      <div className="search__input">
        <Search handleInputSearch={handleInputSearch} />
        <button className="search__show-more btn btn-info" onClick={showMore}>
          show more search options
        </button>
        {showMoreSearchOption ? (
          <div className="search-more">
            <SortDescending
              passSortValue={(sortOption) => getSortValue(sortOption)}
            />
            <SortRange passYear={(year) => getYear(year)} />
            <div className="gener-select">
              <h5 className="gener-select__title">by gener</h5>
              <select
                id="gener-select__id"
                name="gener"
                value={geners}
                onChange={handleChange}
              >
                <option>{selectedGeners.gener}</option>
                {geners.map((gener) => (
                  <option key={gener}>{gener}</option>
                ))}
              </select>
            </div>
          </div>
        ) : null}
      </div>
      <Catalog
        movies={inputSearch}
        openPopup={openPopup}
        closePopup={closePopup}
        addToWatch={addToWatch}
      />
    </div>
  );
}

export default App;
